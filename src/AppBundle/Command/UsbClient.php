<?php

namespace AppBundle\Command;

use AppBundle\Amqp\RpcClient;
use AppBundle\Converter\TramConverter;
use Monolog\Logger;
use PHPMake\SerialPort;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UsbClient extends Command
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SerialPort
     */
    private $port;

    /**
     * @var integer
     */
    private $bufferSize;

    /**
     * @var TramConverter
     */
    private $converter;

    /**
     * @var RpcClient
     */
    private $client;

    public function __construct(LoggerInterface $logger, $bufferSize, TramConverter $converter, RpcClient $client)
    {
        $this->logger = $logger;
        $this->bufferSize = $bufferSize;
        $this->converter = $converter;
        $this->client = $client;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:usb:start')
            ->addArgument('device', InputArgument::REQUIRED, 'the device to be used');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->port = new SerialPort();
        try {
            $this->logger->debug('Setting device');
            $this->port->open($input->getArgument('device'));

            $this->logger->debug('Setting baud rate');
            $this->port->setBaudRate(SerialPort::BAUD_RATE_9600);

            $this->logger->debug('Setting flow control');
            $this->port->setFlowControl(SerialPort::FLOW_CONTROL_NONE);

            $this->logger->debug('Setting canonical mode');
            $this->port->setCanonical(true);
        } catch (\Exception $exception) {
            $output->writeln(sprintf('<error>%s</error>', $exception->getMessage()));
        }

        while (true) {
            try {
                while ($this->port->read(1) !== chr(0x02));
                $buffer = [];
                while (($byte = $this->port->read(1)) !== chr(0x03) || count($buffer) < $this->bufferSize) {
                    $buffer[] = $byte;
                }

                if (count($buffer) == $this->bufferSize) {
                    continue;
                }

                $tram = implode('', $buffer) . chr(0x03);
                list($positionX, $positionY) = $this->converter->convert(
                    TramConverter::DATA_TYPE_REQUEST_ENTRY,
                    $tram
                );
                $this->logger->debug('new position received', ['positionX' => $positionX, 'positionY' => $positionY]);

                $laserData = $this->client->call(
                    $this->converter->convert(
                        TramConverter::DATA_TYPE_REQUEST_POSITION,
                        [$positionX, $positionY]
                    )
                );

                $this->port->write(chr(0x02) . substr($laserData, 1));

            } catch (\Exception $exception) {
                $output->writeln(sprintf('<error>%s</error>', $exception->getMessage()));
            }
        }
    }

    public function __destruct()
    {
        if ($this->port->isOpen()) {
            $this->port->close();
        }
    }

}
