<?php

namespace AppBundle\Converter;

class TramConverter
{
    const DATA_TYPE_REQUEST_POSITION = 0;

    const DATA_TYPE_REQUEST_ENTRY = 1;

    public function convert($dataType, $data)
    {
        switch ($dataType) {
            case static::DATA_TYPE_REQUEST_POSITION:
                return $this->convertPositionRequest($data);
            case static::DATA_TYPE_REQUEST_ENTRY:
                return $this->convertPositionEntry($data);
        }
    }

    private function convertPositionEntry($data)
    {
        return array_map('floatval', explode(chr(0x1f), substr($data, 0, -1)));
    }

    private function convertPositionRequest(array $data)
    {
        return sprintf('%s%s', implode(chr(0x1f), $data), chr(0x03));
    }
}
